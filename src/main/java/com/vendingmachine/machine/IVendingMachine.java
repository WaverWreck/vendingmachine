package com.vendingmachine.machine;

import com.vendingmachine.bank.Money;
import com.vendingmachine.machine.product.Product;

import java.util.Queue;

public interface IVendingMachine {
	public Product dispenceProduct(int shelfCode);
	
	public boolean payProductPrice(int shelfCode, double amountPayed);
	
	public Queue<Money> computeChange(double amountReceived, double amountExpected);
	
	public boolean validateAmountReceived(Queue<Money> moneyReceived);
}
